# vlent.nl website

This repository contains the code to host the vlent.nl website.

The domain used to host my weblog, but I've moved that to
[markvanlent.dev](https://markvanlent.dev). This site only has a few files left
(e.g. to prove that this is my website to Google and Keybase.io) but otherwise
it redirects requests to the new domain.
