FROM nginx:mainline-alpine

# Use my custom Nginx configuration
COPY nginx.conf /etc/nginx/conf.d/default.conf

# Copy some leftover files for this domain
COPY assets/* /var/www/

ARG BUILD_REF
ARG BUILD_DATE
LABEL org.label-schema.schema-version="1.0" \
      org.label-schema.url="https://www.vlent.nl/" \
      org.label-schema.vcs-url="https://gitlab.com/markvl/vlent_nl" \
      org.label-schema.vcs-ref=$BUILD_REF \
      org.label-schema.build-date=$BUILD_DATE \
      maintainer=""
