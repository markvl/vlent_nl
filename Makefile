REGISTRY ?= registry.gitlab.com/markvl
TAG := $(or ${CI_COMMIT_REF_SLUG},latest)
VCS_REF="$$(git rev-parse HEAD)"
BUILD_DATE="$$(date -u +'%Y-%m-%dT%H:%M:%SZ')"

image:
	docker build \
		-f Dockerfile \
		--build-arg BUILD_REF=$(VCS_REF) \
		--build-arg BUILD_DATE=$(BUILD_DATE) \
		-t $(REGISTRY)/vlent_nl:$(TAG) \
		.
